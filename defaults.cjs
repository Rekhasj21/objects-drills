function defaults(obj, defaultProps) {
    if (Array.isArray(obj) && obj !== []) {
        return obj;
    }
    if (obj === null || obj === undefined) {
        return defaultProps
    }
    if (typeof obj !== "object" || Array.isArray(obj)) {
        return [];
    }
    if (typeof obj === "string") {
        const newObj = {}
        for (let index = 0; index < obj.length; index++) {
            newObj[index] = obj[index]
        }
        obj = newObj;
    }
    for (let key in defaultProps) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}

module.exports = defaults