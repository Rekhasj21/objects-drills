function keys(obj) {
  if (typeof obj !== 'object' || obj === null) {
    return []
  }
  let array = []
  for (let key in obj) {
    array.push(key)
  }
  return array
}

module.exports = keys