
function values(obj) {
    if (obj === null || typeof obj !== "object" ){
        return []
    }
    let array =[]
    for (let key in obj){
        array.push(obj[key])
    }
    return array
}

module.exports = values