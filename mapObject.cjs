function mapObject(obj , cb) {
   if (obj === null || typeof obj === undefined || typeof cb !== 'function'){
    return {}
   }
  let object={}
  for (let key in obj){
    object[key] = cb(obj[key],key)
  }
return object
}
  
module.exports = mapObject